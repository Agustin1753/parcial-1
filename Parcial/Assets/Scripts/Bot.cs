using UnityEngine;
public class Bot : MonoBehaviour
{
    private int hp;
    void Start() 
    {
        randomOffset = Random.Range(0f, 2f);
        hp = 100; 
    }
    public void recibirDaņo() 
    { 
        hp = hp - 25;
        if (hp <= 0) 
        {
            this.desaparecer(); 
        } 
    }
    private void desaparecer() 
    {
        Destroy(gameObject); 
    }
    public float rapidez = .5f; 
    public float strength = 9f;
    private float randomOffset; 

    void Update() 
    { Vector3 pos = transform.position;
        pos.x = Mathf.Sin(Time.time * rapidez + randomOffset) * strength;
        transform.position = pos; 
    } 
}