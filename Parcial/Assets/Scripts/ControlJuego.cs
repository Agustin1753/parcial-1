using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject personaje;
    public GameObject pantallagameover;
    public TMPro.TMP_Text textocontadorconometro;


    float tiempoRestante;

    void Start()
    {
        StartCoroutine(ComenzarCronometro(60));
        Time.timeScale = 1;

    }

    void Update()
    {
        setearTextos();

        if (tiempoRestante == 0)
        {
            pantallagameover.SetActive(true);
            Time.timeScale = 0;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }




    public IEnumerator ComenzarCronometro(float valorCronometro = 60)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
    private void setearTextos()
    {
        textocontadorconometro.text = "Tiempo: " + tiempoRestante.ToString();
    }
}